#!/usr/bin/env bash

# Helper script to build images

set -e

function log_info() {
  echo -e "\033[1;35m$1\033[0m"
}

function log_error() {
  echo -e "\033[1;31m$1\033[0m"
}

function clone_core_repo() {
  git clone https://github.com/dependabot/dependabot-core.git --depth 1 --branch "$1" --recurse-submodules
}

declare -A image_types=(
  ["bundler"]="bundler"
  ["npm"]="npm_and_yarn"
  ["gomod"]="go_modules"
  ["pip"]="python"
  ["docker"]="docker"
  ["composer"]="composer"
  ["pub"]="pub"
  ["cargo"]="cargo"
  ["nuget"]="nuget"
  ["maven"]="maven"
  ["gradle"]="gradle"
  ["mix"]="hex"
  ["terraform"]="terraform"
  ["elm"]="elm"
  ["gitsubmodule"]="git_submodules"
  ["github-actions"]="github_actions"
  ["swift"]="swift"
  ["devcontainers"]="devcontainers"
  ["bun"]="bun"
  ["docker-compose"]="docker_compose"
)

type=$1

arch="${ARCH:-$(uname -m)}"
version="$(echo `awk '/"dependabot-omnibus"/ {print $3}' Gemfile | sed 's/["]//g'`)"
image_prefix="${CI_REGISTRY_IMAGE:-registry.gitlab.com/dependabot-gitlab/core-images}"
cache_ref="${image_prefix}/cache:${type}-${arch}"

if [ -z "$CI" ]; then
  prefix=${2:-/dev/}
  image="${image_prefix}${prefix}"
else
  image="${image_prefix}/${PREFIX}"
  git config --global advice.detachedHead false
fi

tag="${version}-${arch}"

if [ -d "dependabot-core" ]; then
  log_info "Cleaning up dependabot-core directory"

  git_tag="v$version"
  cd dependabot-core
  git checkout .
  git fetch origin +refs/tags/$git_tag:refs/tags/$git_tag
  git checkout tags/$git_tag
  cd ..
  echo ""
else
  log_info "Cloning dependabot-core repository"
  clone_core_repo "v$version"
  echo ""
fi

if [ "$type" == "core" ]; then
  dockerfile="dependabot-core/Dockerfile.updater-core"
else
  dockerfile=dependabot-core/$(echo ${image_types["$type"]})/Dockerfile
  log_info "Updating $dockerfile"
  sed -i.bak "s#ghcr.io/dependabot/dependabot-updater-core#${image}core:${tag}#g" $dockerfile && rm $dockerfile.bak
  echo -e "Following dockerfile will be used for build\n"
  echo "============================================="
  cat $dockerfile
  echo -e "=============================================\n"
fi

log_info "Building $type image"
docker buildx build \
  --platform linux/${arch} \
  --file $dockerfile \
  --cache-from type=registry,ref=${cache_ref} \
  --cache-to type=registry,ref=${cache_ref},mode=max,ignore-error=true \
  --tag ${image}${type}:${tag} \
  --provenance false \
  --push \
  dependabot-core
