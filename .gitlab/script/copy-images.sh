#!/bin/bash

# Script for image release on CI
#
set -e

version="$CORE_VERSION"
source="$CI_REGISTRY_IMAGE"
destination="ghcr.io/dependabot-gitlab/dependabot-updater"
image_types=(
  core
  bundler
  npm
  gomod
  pip
  docker
  composer
  pub
  cargo
  nuget
  maven
  gradle
  mix
  terraform
  elm
  gitsubmodule
  swift
  devcontainers
  bun
  docker-compose
)

function log_with_header() {
  length=$(echo "$1" | awk '{print length}')
  delimiter=$(head -c $length </dev/zero | tr '\0' "${2:-=}")

  log_info "$delimiter"
  log_info "$1"
  log_info "$delimiter"
}

function log_info() {
  echo -e "\033[1;35m$1\033[0m"
}

function log_success() {
  echo -e "\033[1;32m$1\033[0m"
}

function skopeo_copy() {
  skopeo copy -q --multi-arch all "docker://$1" "docker://$2"
}

for type in "${image_types[@]}"; do
  gl_image="$source/$type:$version"
  gh_image="$destination-$type"

  log_with_header "Copying '$type' image to '$gh_image'"
  skopeo_copy "$gl_image" "$gh_image:$version"
  skopeo_copy "$gl_image" "$gh_image:latest"
  log_success "Copied image '$gh_image:$version' successfully!"
done
